import 'package:flutter/material.dart';

class Demo extends StatefulWidget {
  @override
  _DemoState createState() => _DemoState();
}

class _DemoState extends State<Demo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(
         // automaticallyImplyLeading: false,
        backgroundColor: Colors.yellow,
        leading:  Center(//change
          child: CircleAvatar(
            backgroundImage: AssetImage('assets/images/pink.jpg'),
            radius: 25,
          ),
        ),
        // actions: [
        //   Padding(
        //     padding: const EdgeInsets.only(left:10),
        //     child: Row(
        //       children: [
        //         CircleAvatar(
        //           backgroundImage: AssetImage('assets/images/pink.jpg'),
        //           radius: 30,
        //         ),
        //       ],
        //     ),
        //   )
        // ],
      ),
      body: SingleChildScrollView(
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(Icons.arrow_back),
          ),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 120, top: 30),
                child: CircleAvatar(
                  backgroundImage: AssetImage('assets/images/pink.jpg'),
                  radius: 60,
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 20, 60, 80),
                child: CircleAvatar(
                    backgroundColor: Colors.white, child: Icon(Icons.add)),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 20.0),
                child: Row(children: [
                  IconButton(
                    icon: Icon(Icons.edit),
                    onPressed: () {
                      setState(() {});
                    },
                  ),
                  Text('Edit'),
                ]),
              ),
            ],
          ),
          Container(
            alignment: Alignment.topLeft,
            decoration: BoxDecoration(
                border:
                    Border(bottom: BorderSide(color: Colors.grey, width: 3.0))),
            height: 100,
            width: 350,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Row(
                    children: [
                      Icon(Icons.person),
                      Text('Mark Shaw'),
                    ],
                  ),
                  Row(
                    children: [
                      Icon(Icons.inbox),
                      Text('sales@shawmansoftware.com'),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    // crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 2.0),
                        child: Row(
                          children: [
                            Icon(Icons.mobile_screen_share),
                            Text('9789876533'),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 3.0),
                        child: Row(
                          children: [
                            Icon(Icons.calendar_today),
                            Text('01/01/1990'),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),

          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                Card(
                  elevation: 4.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                          'Tell us about your \nallergies,preferences & likes.'),
                      Container(
                        height: 80,
                        width: 100,
                        child: FlatButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          // height: 80,
                          // minWidth: 30,
                          color: Colors.yellow,
                          child: Text(
                            'Edit Profile',
                          ),
                          onPressed: () {
                            setState(() {});
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                Card(
                  elevation: 4.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Add new or edit existing addresses'),
                      Container(
                        height: 80,
                        width: 100,
                        child: FlatButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          // height: 80,
                          color: Colors.yellow,
                          child: Text(
                            'Adress Diary',
                          ),
                          onPressed: () {
                            setState(() {});
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                Card(
                  elevation: 4.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Your history with us!'),
                      Container(
                        height: 80,
                        width: 100,
                        child: FlatButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          // height: 80,
                          color: Colors.yellow,
                          child: Text(
                            'Timeline',
                          ),
                          onPressed: () {
                            setState(() {});
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                Card(
                  elevation: 4.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Loyality points and Offers'),
                      Container(
                        height: 80,
                        width: 100,
                        child: FlatButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          // height: 80,
                          color: Colors.yellow,
                          child: Text(
                            'Loyality and Offers',
                          ),
                          onPressed: () {
                            setState(() {});
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Center(
            child: FlatButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              // height: 30,
              color: Colors.yellow,
              child: Text(
                'Log out',
              ),
              onPressed: () {
                setState(() {});
              },
            ),
          ),
        ]),
      ),
      
    );
  }
}